export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  TabOne: undefined;
  TabTwo: undefined;
  Principal: undefined;
  Estadisticas: undefined;
  Calendario: undefined;
};

export type TabOneParamList = {
  TabOneScreen: undefined;
};

export type TabTwoParamList = {
  TabTwoScreen: undefined;
};

export type PrincipalParamList = {
  PrincipalScreen: undefined;
};

export type EstadisticasParamList = {
  EstadisticasScreen: undefined;
};

export type CalendarioParamList = {
  CalendarioScreen: undefined;
};
