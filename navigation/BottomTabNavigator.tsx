import { Ionicons } from "@expo/vector-icons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import * as React from "react";

import Colors from "../constants/Colors";
import useColorScheme from "../hooks/useColorScheme";
import TabOneScreen from "../screens/TabOneScreen";
import TabTwoScreen from "../screens/TabTwoScreen";
import PrincipalScreen from "../screens/custom-screens/PrincipalScreen";
import EstadisticasScreen from "../screens/custom-screens/EstadisticasScreen";
import CalendarioScreen from "../screens/custom-screens/CalendarioScreen";
import {
  BottomTabParamList,
  TabOneParamList,
  TabTwoParamList,
  PrincipalParamList,
  EstadisticasParamList,
  CalendarioParamList,
} from "../types";

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="TabOne"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}
    >
      <BottomTab.Screen
        name="TabOne"
        component={TabOneNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <TabBarIcon name="ios-code" color={color} />
          ),
        }}
      />
      <BottomTab.Screen
        name="TabTwo"
        component={TabTwoNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <TabBarIcon name="ios-code" color={color} />
          ),
        }}
      />
      <BottomTab.Screen
        name="Principal"
        component={PrincipalNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <TabBarIcon name="ios-code" color={color} />
          ),
        }}
      />
      <BottomTab.Screen
        name="Estadisticas"
        component={EstadisticasNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <TabBarIcon name="ios-code" color={color} />
          ),
        }}
      />
      <BottomTab.Screen
        name="Calendario"
        component={CalendarioNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <TabBarIcon name="ios-code" color={color} />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: string; color: string }) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabOneStack = createStackNavigator<TabOneParamList>();

function TabOneNavigator() {
  return (
    <TabOneStack.Navigator>
      <TabOneStack.Screen
        name="TabOneScreen"
        component={TabOneScreen}
        options={{ headerTitle: "Tab One Title" }}
      />
    </TabOneStack.Navigator>
  );
}

const TabTwoStack = createStackNavigator<TabTwoParamList>();

function TabTwoNavigator() {
  return (
    <TabTwoStack.Navigator>
      <TabTwoStack.Screen
        name="TabTwoScreen"
        component={TabTwoScreen}
        options={{ headerTitle: "Tab Two Title" }}
      />
    </TabTwoStack.Navigator>
  );
}

const PrincipalStack = createStackNavigator<PrincipalParamList>();
function PrincipalNavigator() {
  return (
    <PrincipalStack.Navigator>
      <PrincipalStack.Screen
        name="PrincipalScreen"
        component={PrincipalScreen}
        options={{ headerTitle: "Principal Title" }}
      />
    </PrincipalStack.Navigator>
  );
}

const EstadisticasStack = createStackNavigator<EstadisticasParamList>();
function EstadisticasNavigator() {
  return (
    <EstadisticasStack.Navigator>
      <EstadisticasStack.Screen
        name="EstadisticasScreen"
        component={EstadisticasScreen}
        options={{ headerTitle: "Estadisticas Title" }}
      />
    </EstadisticasStack.Navigator>
  );
}


const CalendarioStack = createStackNavigator<CalendarioParamList>();
function CalendarioNavigator() {
  return (
    <CalendarioStack.Navigator>
      <CalendarioStack.Screen
        name="CalendarioScreen"
        component={CalendarioScreen}
        options={{ headerTitle: "Calendario Title" }}
      />
    </CalendarioStack.Navigator>
  );
}
