import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          TabOne: {
            screens: {
              TabOneScreen: 'one',
            },
          },
          TabTwo: {
            screens: {
              TabTwoScreen: 'two',
            },
          },
          Principal: {
            screens: {
              PrincipalScreen: 'principal',
            },
          },
          Estadisticas: {
            screens: {
              EstadisticasScreen: 'estadisticas',
            },
          },
          Calendario: {
            screens: {
              CalendarioScreen: 'calendario',
            },
          },
        },
      },
      NotFound: '*',
    },
  },
};
